﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HellgroundArenaLog.BO.DTO;

namespace HellgroundArenaLog.BO
{
    internal class Top100Comparer
    {
        private List<ArenaTeam> previousArenaTeams;

        public List<ArenaTeam> CurrentlyPlayingTeams;
        
        public Top100Comparer(List<ArenaTeam> top100)
        {
            this.previousArenaTeams = top100;
        }

        public void CompareWithPrevious(List<ArenaTeam> currentTop100)
        {
            this.CurrentlyPlayingTeams = new List<ArenaTeam>();

            List<ArenaTeam> bySeasonPlayedDiff = CompareBySeasonPlayed(currentTop100);
            List<ArenaTeam> newTeams = FindNewTeams(currentTop100);
            
            this.CurrentlyPlayingTeams.AddRange(bySeasonPlayedDiff);
            this.CurrentlyPlayingTeams.AddRange(newTeams);

            this.previousArenaTeams = currentTop100;
        }

        private List<ArenaTeam> CompareBySeasonPlayed(List<ArenaTeam> currentTop100)
        {
            var result = new List<ArenaTeam>();

            foreach (var arenaTeam in currentTop100)
            {
                if (this.previousArenaTeams.Exists(t => t.Name == arenaTeam.Name))
                {
                    var previousTeam = previousArenaTeams.Find(t => t.Name == arenaTeam.Name);
                    if (previousTeam.SeasonGamesPlayed != arenaTeam.SeasonGamesPlayed)
                    {
                        arenaTeam.RateChange = arenaTeam.Rating - previousTeam.Rating;
                        arenaTeam.WhatChanged += string.Format("SeasonPlayed was: {0} is {1}; ",
                            previousTeam.SeasonGamesPlayed, arenaTeam.SeasonGamesPlayed);
                        result.Add(arenaTeam);
                    }
                }
            }

            return result;
        }

        private List<ArenaTeam> FindNewTeams(List<ArenaTeam> currentTop100)
        {
            var result = currentTop100.FindAll(t => !this.previousArenaTeams.Exists(p => p.Name == t.Name));

            foreach (var arenaTeam in result)
            {
                arenaTeam.WhatChanged += "New team ";
            }

            return result;
        }

    }
}

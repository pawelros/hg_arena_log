﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HellgroundArenaLog.BO.DTO;

namespace HellgroundArenaLog.BO.Providers
{
    internal interface IProvider
    {
        List<ArenaTeam> Retrieve();
    }
}

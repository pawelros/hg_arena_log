﻿using HellgroundArenaLog.BO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HellgroundArenaLog.BO.Providers.ArmoryScanner
{
    internal class ArenaTeamDataAdapter
    {
        private readonly ArenaTeam arenaTeam;
        private const string TeamUrl = "http://armory.hellground.net/team-info.xml";

        #region xml attribute names

        private const string battleGroupAttrName = "battleGroup";
        private const string createdAttrName = "created";
        private const string factionAttrName = "faction";
        private const string gamesPlayedAttrName = "gamesPlayed";
        private const string gamesWonAttrName = "gamesWon";
        private const string nameAttrName = "name";
        private const string rankingAttrName = "ranking";
        private const string ratingAttrName = "rating";
        private const string realmAttrName = "realm";
        private const string realmUrlAttrName = "realmUrl";
        private const string seasonGamesPlayedAttrName = "seasonGamesPlayed";
        private const string seasonGamesWonAttrName = "seasonGamesWon";
        private const string teamSizeAttrName = "teamSize";
        private const string teamUrlAttrName = "teamUrl";

        private const string charUrlAttrName = "charUrl";
        private const string classAttrName = "classId";
        private const string personalRatingAttrName = "contribution";
        private const string weekGamesPlayedAttrName = "gamesPlayed";
        private const string weekGamesWonAttrName = "gamesWon";
        private const string guildAttrName = "guild";
        private const string nickNameAttrName = "name";
        private const string charRaceAttrName = "raceId";

        private List<Player> members;


        #endregion

        public ArenaTeamDataAdapter(ArenaTeam arenaTeam)
        {
            this.arenaTeam = arenaTeam;
        }

        public void Adapt(XElement xElement)
        {
            arenaTeam.BattleGroup = xElement.Attribute(battleGroupAttrName).Value;
            arenaTeam.Created = xElement.Attribute(createdAttrName).Value;
            arenaTeam.Faction = xElement.Attribute(factionAttrName).Value;
            arenaTeam.GamesPlayed = xElement.Attribute(gamesPlayedAttrName).Value;
            arenaTeam.GamesWon = xElement.Attribute(gamesWonAttrName).Value;
            arenaTeam.Name = xElement.Attribute(nameAttrName).Value;
            arenaTeam.Ranking = xElement.Attribute(rankingAttrName).Value;
            arenaTeam.Rating = Int32.Parse(xElement.Attribute(ratingAttrName).Value);
            arenaTeam.Realm = xElement.Attribute(realmAttrName).Value;
            arenaTeam.RealmUrl = xElement.Attribute(realmUrlAttrName).Value;
            arenaTeam.SeasonGamesPlayed = Int32.Parse(xElement.Attribute(seasonGamesPlayedAttrName).Value);
            arenaTeam.SeasonGamesWon = xElement.Attribute(seasonGamesWonAttrName).Value;
            arenaTeam.TeamSize = xElement.Attribute(teamSizeAttrName).Value;
            arenaTeam.TeamUrl = xElement.Attribute(teamUrlAttrName).Value;
            arenaTeam.Url = ParseUrl();
        }

        public void AdaptTeamDetails(XElement xElement)
        {
            var members = xElement.Elements("members").Elements();
            this.arenaTeam.Members = new List<Player>();

            foreach (var member in members)
            {
                Player player = new Player();

                try
                {
                    player.CharUrl = member.Attribute(charUrlAttrName).Value;
                    player.Class = (Player.Type) (Int32.Parse(member.Attribute(classAttrName).Value));
                    player.PersonalRating = Int32.Parse(member.Attribute(personalRatingAttrName).Value);
                    player.WeekGamesPlayed = Int32.Parse(member.Attribute(weekGamesPlayedAttrName).Value);
                    player.WeekGamesWon = Int32.Parse(member.Attribute(weekGamesWonAttrName).Value);
                    player.Guild = member.Attribute(guildAttrName).Value;
                    player.NickName = member.Attribute(nickNameAttrName).Value;
                    player.CharRace = (Player.Race) (Int32.Parse(member.Attribute(charRaceAttrName).Value));
                    player.SeasonGamesPlayed = Int32.Parse(member.Attribute(seasonGamesPlayedAttrName).Value);
                    player.SeasonGamesWon = Int32.Parse(member.Attribute(seasonGamesWonAttrName).Value);
                }
                catch (Exception ex)
                {
                   Console .WriteLine(ex.ToString());
                }

               this.arenaTeam.Members.Add(player);
            }
        }


        internal string ParseUrl()
        {
            return string.Concat(TeamUrl, '?', this.arenaTeam.RealmUrl);
        }
    }
}

﻿
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Xml.Linq;
using HellgroundArenaLog.BO.Providers;
using HellgroundArenaLog.BO.DTO;
using System.Collections.Generic;
using System.Xml;
using System.IO;
namespace HellgroundArenaLog.BO.Providers.ArmoryScanner
{
    internal class ArmoryScanner : IProvider
    {
        private List<ArenaTeam> arenaTeamList;
        private WebArmoryClient client;

        #region IProvider Members

        public List<ArenaTeam> Retrieve()
        {
            string downloadedData = DownloadData();
            ParseResponse(downloadedData);

            return this.arenaTeamList;
        }

        #endregion

        public void RetrieveTeamDetails(ArenaTeam arenaTeam)
        {
            string response = this.client.DownloadTeamDetails(arenaTeam);

            XElement root = XElement.Parse(response)
                .Elements()
                .First()
                .Elements()
                .First();
           
            ArenaTeamDataAdapter adapter = new ArenaTeamDataAdapter(arenaTeam);
            adapter.AdaptTeamDetails(root);
        }

        private string DownloadData()
        {
            this.arenaTeamList = new List<ArenaTeam>();
            this.client = CreateWebClient();
            return client.DownloadTop100();
        }

        protected virtual WebArmoryClient CreateWebClient()
        {
            return new WebArmoryClient();
        }

        private void ParseResponse(string response)
        {
            this.arenaTeamList = new List<ArenaTeam>();
            XElement root = XElement.Parse(response);
            IEnumerable<XElement> teams = root.Elements("arenaLadderPagedResult").First().Elements("arenaTeams").First().Elements("arenaTeam");

            foreach (XElement team in teams)
            {
                ArenaTeam arenaTeam = new ArenaTeam();
                ArenaTeamDataAdapter adapter = new ArenaTeamDataAdapter(arenaTeam);
                adapter.Adapt(team);
                arenaTeamList.Add(arenaTeam);
            }
        }
    }
}

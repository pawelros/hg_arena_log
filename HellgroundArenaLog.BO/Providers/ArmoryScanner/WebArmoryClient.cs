﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Net;
using HellgroundArenaLog.BO.DTO;

namespace HellgroundArenaLog.BO.Providers.ArmoryScanner
{
    internal class WebArmoryClient : WebClient
    {
        private const string HgArmoryUrl = "http://armory.hellground.net/arena-ladder.xml?ts=2&b=HellGround";
        private const string TeamUrl = "http://armory.hellground.net/team-info.xml?b=HellGround";
        private const int Timeout = 20 * 60 * 1000;

        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = Timeout;
            return w;
        }

        public virtual string DownloadTop100()
        {
            WebClient client = new WebClient();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Console.WriteLine("Downloading top 100");
            string result = client.DownloadString(HgArmoryUrl);
            stopwatch.Stop();
            Console.WriteLine("Downloading top 100 taken: "+stopwatch.Elapsed.TotalSeconds);
            return result;
        }

        public virtual string DownloadTeamDetails(ArenaTeam team)
        {
            WebClient client = new WebClient();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Console.WriteLine("Downloading details for team:"+team.Name);
            var result = client.DownloadString(team.Url);
            stopwatch.Stop();
            Console.WriteLine("Downloading details for team taken:" +stopwatch.Elapsed.TotalSeconds);
            return result;
        }
    }
}

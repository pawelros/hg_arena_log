﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HellgroundArenaLog.BO.DTO
{
    internal class Player
    {
        public enum Type
        {
            Warrior = 1,
            Paladin = 2,
            Hunter = 3,
            Rogue = 4,
            Priest = 5,
            Shaman = 7,
            Mage = 8,
            Warlock = 9,
            Druid = 11
        };

        public enum Race
        {
            Human = 1,
            Orc = 2,
            Dwarf = 3,
            NightElf = 4,
            Undead = 5,
            Tauren = 6,
            Gnome = 7,
            Troll = 8,
            BloodElf = 10,
            Draenei = 11,
        };


        public string CharUrl { get; set; }
        public Type Class { get; set; }
        public int PersonalRating { get; set; }
        public int WeekGamesPlayed { get; set; }
        public int WeekGamesWon { get; set; }
        public string Guild { get; set; }
        public string NickName { get; set; }
        public Race? CharRace { get; set; }
        public int SeasonGamesPlayed { get; set; }
        public int SeasonGamesWon { get; set; }
    }
}

﻿
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Xml.Linq;

namespace HellgroundArenaLog.BO.DTO
{
    internal class ArenaTeam
    {

        #region Public fields

        public string BattleGroup { get; set; }
        public string Created { get; set; }
        public string Faction { get; set; }
        public string GamesPlayed { get; set; }
        public string GamesWon { get; set; }
        public string Name { get; set; }
        public string Ranking { get; set; }
        public int Rating { get; set; }
        public string Realm { get; set; }
        public string RealmUrl { get; set; }
        public int SeasonGamesPlayed { get; set; }
        public string SeasonGamesWon { get; set; }
        public string TeamSize { get; set; }
        public string TeamUrl { get; set; }
        public string Url { get; set; }
        public List<Player> Members { get; set; }

        public int RateChange { get; set; }
        public string WhatChanged { get; set; }
    

    #endregion

    }
}

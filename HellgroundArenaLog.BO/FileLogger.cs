﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HellgroundArenaLog.BO
{
    public static class FileLogger
    {
        public const string LogPath = @"d:\Projekty\var\log\HellgroundArenaLog\";

        public static void WriteDebugMessage(string message)
        {
            if (!Directory.Exists(LogPath))
            {
                Directory.CreateDirectory(LogPath);
            }

            StreamWriter log;

            string filename = string.Concat(LogPath, "debug", DateTime.Now.ToString("yyyyMMdd"), ".txt");

            if (!File.Exists(filename))
            {
                log = new StreamWriter(filename);
            }
            else
            {
                log = File.AppendText(filename);
            }

            // Write to the file:
            log.Write(DateTime.Now.ToString("HH:mm:ss"));
            log.Write(" ");
            log.WriteLine(message);

            // Close the stream:
            log.Close();
        }

        public static void WriteLogMessage(string message)
        {
            if (!Directory.Exists(LogPath))
            {
                Directory.CreateDirectory(LogPath);
            }

            StreamWriter log;

            string filename = string.Concat(LogPath, "log", DateTime.Now.ToString("yyyyMMdd"), ".txt");

            if (!File.Exists(filename))
            {
                log = new StreamWriter(filename);
            }
            else
            {
                log = File.AppendText(filename);
            }

            // Write to the file:
            log.Write(DateTime.Now.ToString("HH:mm:ss"));
            log.Write(" ");
            log.WriteLine(message);

            // Close the stream:
            log.Close();
        }

        public static void WriteException(Exception ex)
        {
            if (!Directory.Exists(LogPath))
            {
                Directory.CreateDirectory(LogPath);
            }

            StreamWriter log;

            string filename = string.Concat(LogPath, "exception", DateTime.Now.ToString("yyyyMMdd"), ".txt");

            if (!File.Exists(filename))
            {
                log = new StreamWriter(filename);
            }
            else
            {
                log = File.AppendText(filename);
            }

            // Write to the file:
            log.Write(DateTime.Now.ToString("HH:mm:ss"));
            log.Write(" ");
            log.WriteLine(ex);

            // Close the stream:
            log.Close();
        }
    }
}

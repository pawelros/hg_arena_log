﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using HellgroundArenaLog.BO.DTO;
using HellgroundArenaLog.BO.Providers.ArmoryScanner;

namespace HellgroundArenaLog.Tester
{
    class Program
    {
        static void Main(string[] args)
        {
          

            ArmoryScanner scanner = new ArmoryScanner();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var top100 = scanner.Retrieve();

            ArenaTeam x = top100[54];

            scanner.RetrieveTeamDetails(x);

            stopwatch.Stop();

            Console.WriteLine("total time elapsed: "+stopwatch.Elapsed.Seconds);



            Console.ReadKey();
            

        }
    }
}

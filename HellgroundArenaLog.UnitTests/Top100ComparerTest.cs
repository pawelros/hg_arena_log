﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using HellgroundArenaLog.BO;
using HellgroundArenaLog.BO.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HellgroundArenaLog.UnitTests
{
    [TestClass]
    public class Top100ComparerTest
    {
        [TestMethod, Owner("Rosiv")]
        public void Should_Compare_Old_And_New_Top_100_By_Season_Games_Played()
        {
            List<ArenaTeam> oldTop100 = new List<ArenaTeam>();
            ArenaTeam team1 = new ArenaTeam()
            {
                Name = "Lazy noobs",
                SeasonGamesPlayed = 30
            };
            ArenaTeam team2 = new ArenaTeam()
            {
                Name = "No lifers",
                SeasonGamesPlayed = 250
            };
            ArenaTeam team3 = new ArenaTeam()
            {
                Name = "Just for AP",
                SeasonGamesPlayed = 10
            };
            ArenaTeam team4 = new ArenaTeam()
            {
                Name = "Looking for a mate",
                SeasonGamesPlayed = 0
            };

            oldTop100.Add(team1);
            oldTop100.Add(team2);
            oldTop100.Add(team3);
            oldTop100.Add(team4);

            Top100Comparer comparer = new Top100Comparer(oldTop100);

            ArenaTeam newTeam2 = new ArenaTeam()
            {
                Name = "No lifers",
                SeasonGamesPlayed = 300
            };
            ArenaTeam newTeam3 = new ArenaTeam()
            {
                Name = "Just for AP",
                SeasonGamesPlayed = 20
            };

            List<ArenaTeam> newTop100 = new List<ArenaTeam>();
            newTop100.Add(team1);
            newTop100.Add(newTeam2);
            newTop100.Add(newTeam3);
            newTop100.Add(team4);

            comparer.CompareWithPrevious(newTop100);

            List<ArenaTeam> diff = comparer.CurrentlyPlayingTeams;

            Assert.AreEqual(2, diff.Count);
            Assert.AreEqual("No lifers", diff[0].Name);
            Assert.AreEqual(300, diff[0].SeasonGamesPlayed);
            Assert.AreEqual("Just for AP", diff[1].Name);
            Assert.AreEqual(20, diff[1].SeasonGamesPlayed);
        }

        [TestMethod, Owner("Rosiv")]
        public void Should_Not_Returns_Teams_That_Were_Not_Present_In_New_Top100()
        {
            List<ArenaTeam> oldTop100 = new List<ArenaTeam>();
            ArenaTeam team1 = new ArenaTeam()
            {
                Name = "Lazy noobs",
                SeasonGamesPlayed = 30
            };
            ArenaTeam team2 = new ArenaTeam()
            {
                Name = "No lifers",
                SeasonGamesPlayed = 250
            };
            ArenaTeam team3 = new ArenaTeam()
            {
                Name = "Just for AP",
                SeasonGamesPlayed = 10
            };
            ArenaTeam team4 = new ArenaTeam()
            {
                Name = "Looking for a mate",
                SeasonGamesPlayed = 0
            };

            oldTop100.Add(team1);
            oldTop100.Add(team2);
            oldTop100.Add(team3);
            oldTop100.Add(team4);

            Top100Comparer comparer = new Top100Comparer(oldTop100);

            List<ArenaTeam> newTop100 = new List<ArenaTeam>();
            newTop100.Add(team1);
            newTop100.Add(team3);

            comparer.CompareWithPrevious(newTop100);

            List<ArenaTeam> diff = comparer.CurrentlyPlayingTeams;

            Assert.AreEqual(0, diff.Count);
        }

        [TestMethod, Owner("Rosiv")]
        public void Should_Returns_Teams_That_Were_Not_Present_In_Previous_Top100()
        {
            List<ArenaTeam> oldTop100 = new List<ArenaTeam>();
            ArenaTeam team1 = new ArenaTeam()
            {
                Name = "Lazy noobs",
                SeasonGamesPlayed = 30
            };
            ArenaTeam team2 = new ArenaTeam()
            {
                Name = "No lifers",
                SeasonGamesPlayed = 250
            };

            ArenaTeam team3 = new ArenaTeam()
          {
              Name = "Just for AP",
              SeasonGamesPlayed = 10
          };
            ArenaTeam team4 = new ArenaTeam()
            {
                Name = "Looking for a mate",
                SeasonGamesPlayed = 0
            };

            oldTop100.Add(team1);
            oldTop100.Add(team2);

            Top100Comparer comparer = new Top100Comparer(oldTop100);

            List<ArenaTeam> newTop100 = new List<ArenaTeam>();
            newTop100.Add(team1);
            newTop100.Add(team2);
            newTop100.Add(team3);
            newTop100.Add(team4);

            comparer.CompareWithPrevious(newTop100);

            List<ArenaTeam> diff = comparer.CurrentlyPlayingTeams;

            Assert.AreEqual(2, diff.Count);
            Assert.AreEqual("Just for AP", diff[0].Name);
            Assert.AreEqual(10, diff[0].SeasonGamesPlayed);
            Assert.AreEqual("Looking for a mate", diff[1].Name);
            Assert.AreEqual(0, diff[1].SeasonGamesPlayed);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using HellgroundArenaLog.BO.DTO;
using System.Collections.Generic;
using HellgroundArenaLog.BO.Providers.ArmoryScanner;
using HellgroundArenaLog.UnitTests.Providers.Stubs;
using System.IO;

namespace HellgroundArenaLog.UnitTests
{
  
    [TestClass()]
    public class ArmoryScannerTest
    {

        [TestMethod(), Owner("Rosiv")]
        [DeploymentItem(@"..\..\Xml\Top100_Response.xml", "top100_test")]
        public void Should_Retrieve_Top_100_Arena_Teams_From_Web_Armory()
        {
            string response = File.ReadAllText(@"top100_test\Top100_Response.xml");
            WebArmoryClientStub client = new WebArmoryClientStub();
            client.Top100Response = response;
            ArmoryScanner scanner = new ArmoryScannerStub(client);
            List<ArenaTeam> actual = scanner.Retrieve();

            Assert.AreEqual(100, actual.Count);

            ArenaTeam rank1 = actual[0];

            Assert.AreEqual("HellGround", rank1.BattleGroup);
            Assert.AreEqual("1234569600000", rank1.Created);
            Assert.AreEqual("Horde", rank1.Faction);
            Assert.AreEqual("10", rank1.GamesPlayed);
            Assert.AreEqual("9", rank1.GamesWon);
            Assert.AreEqual("kana raneil", rank1.Name);
            Assert.AreEqual("1", rank1.Ranking);
            Assert.AreEqual("2203", rank1.Rating);
            Assert.AreEqual("HellGround", rank1.Realm);
            Assert.AreEqual(146, rank1.SeasonGamesPlayed);
            Assert.AreEqual("130", rank1.SeasonGamesWon);
            Assert.AreEqual("2", rank1.TeamSize);
            Assert.AreEqual("http://armory.hellground.net/team-info.xml?b=HellGround&r=HellGround&ts=2&t=kana+raneil&ff=realm&fv=HellGround&select=kana+raneil", rank1.Url);
        }

        [TestMethod(), Owner("Rosiv")]
        [DeploymentItem(@"..\..\Xml\Top100_Response.xml", "should_retrieve_team_details_from_armory")]
        [DeploymentItem(@"..\..\Xml\TeamDetails.xml", "should_retrieve_team_details_from_armory")]
        public void Should_Retrieve_Team_Details_From_Web_Armory()
        {
            string top100Response = File.ReadAllText(@"should_retrieve_team_details_from_armory\Top100_Response.xml");
            string teamDetailsResponse = File.ReadAllText(@"should_retrieve_team_details_from_armory\TeamDetails.xml");
            WebArmoryClientStub client = new WebArmoryClientStub();

            client.Top100Response = top100Response;
            client.TeamDetailsResponse = teamDetailsResponse;
            
            ArmoryScanner scanner = new ArmoryScannerStub(client);
            List<ArenaTeam> actual = scanner.Retrieve();

            Assert.AreEqual(100, actual.Count);

            ArenaTeam rank1 = actual[0];

            scanner.RetrieveTeamDetails(rank1);

            Assert.AreEqual(2, rank1.Members.Count);
        }
    }
}

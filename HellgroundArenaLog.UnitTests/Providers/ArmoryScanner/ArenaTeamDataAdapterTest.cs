﻿using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using HellgroundArenaLog.BO.DTO;
using HellgroundArenaLog.BO.Providers.ArmoryScanner;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HellgroundArenaLog.UnitTests
{
    [TestClass]
    public class ArenaTeamDataAdapterTest
    {
        [TestMethod, Owner("Pro")]
        [DeploymentItem(@"..\..\Xml\Top100_Response.xml", "top100")]
        public void Parse_Url_Test()
        {
            string response = File.ReadAllText(@"top100\Top100_Response.xml");
            XElement root = XElement.Parse(response);
            IEnumerable<XElement> teams = root.Elements("arenaLadderPagedResult").First().Elements("arenaTeams").First().Elements("arenaTeam");
            XElement team = teams.First();

            ArenaTeam arenaTeam = new ArenaTeam();
            ArenaTeamDataAdapter adapter = new ArenaTeamDataAdapter(arenaTeam);

            adapter.Adapt(team);

            string actual = adapter.ParseUrl();
            string expected =
                "http://armory.hellground.net/team-info.xml?b=HellGround&r=HellGround&ts=2&t=kana+raneil&ff=realm&fv=HellGround&select=kana+raneil";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod, Owner("Pro")]
        [DeploymentItem(@"..\..\Xml\TeamDetails.xml", "adapt_team_details")]
        public void Should_Adapt_Team_Details()
        {
            string response = File.ReadAllText(@"adapt_team_details\TeamDetails.xml");
            XElement root = XElement.Parse(response);
            XElement teamDetails = root.Elements().First().Elements().First();

            ArenaTeam arenaTeam = new ArenaTeam();
            ArenaTeamDataAdapter adapter = new ArenaTeamDataAdapter(arenaTeam);

            adapter.AdaptTeamDetails(teamDetails);

            Assert.AreEqual(2, arenaTeam.Members.Count);
            Player member1 = arenaTeam.Members[0];
            Player member2 = arenaTeam.Members[1];

            Assert.AreEqual("r=HellGround&n=Flesh", member1.CharUrl);
            Assert.AreEqual(Player.Type.Warrior, member1.Class);
            Assert.AreEqual(1903, member1.PersonalRating);
            Assert.AreEqual(5, member1.WeekGamesPlayed);
            Assert.AreEqual(4, member1.WeekGamesWon);
            Assert.AreEqual("Valhalla", member1.Guild);
            Assert.AreEqual("Flesh", member1.NickName);
            Assert.AreEqual(Player.Race.Orc, member1.CharRace);
            Assert.AreEqual(96, member1.SeasonGamesPlayed);
            Assert.AreEqual(69, member1.SeasonGamesWon);

            Assert.AreEqual("r=HellGround&n=Rosiv", member2.CharUrl);
            Assert.AreEqual(Player.Type.Paladin, member2.Class);
            Assert.AreEqual(1911, member2.PersonalRating);
            Assert.AreEqual(5, member2.WeekGamesPlayed);
            Assert.AreEqual(4, member2.WeekGamesWon);
            Assert.AreEqual("Valhalla", member2.Guild);
            Assert.AreEqual("Rosiv", member2.NickName);
            Assert.AreEqual(Player.Race.BloodElf, member2.CharRace);
            Assert.AreEqual(95, member2.SeasonGamesPlayed);
            Assert.AreEqual(69, member2.SeasonGamesWon);
        }
    }
}

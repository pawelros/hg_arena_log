﻿using HellgroundArenaLog.BO.DTO;
using HellgroundArenaLog.BO.Providers.ArmoryScanner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HellgroundArenaLog.UnitTests.Providers.Stubs
{
    internal class WebArmoryClientStub : WebArmoryClient
    {
        public string Top100Response { get; set; }
        public string TeamDetailsResponse { get; set; }

        public WebArmoryClientStub()
        {
        }

        public override string DownloadTop100()
        {
            return this.Top100Response;
        }

        public override string DownloadTeamDetails(ArenaTeam team)
        {
            return this.TeamDetailsResponse;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HellgroundArenaLog.BO.Providers.ArmoryScanner;

namespace HellgroundArenaLog.UnitTests.Providers.Stubs
{
    internal class ArmoryScannerStub : ArmoryScanner
    {
        private readonly WebArmoryClient client;

        public ArmoryScannerStub(WebArmoryClient client)
        {
            this.client = client;
        }

        protected override WebArmoryClient CreateWebClient()
        {
            return this.client;
        }
    }
}

﻿using HellgroundArenaLog.Providers.Providers.ArmoryScanner;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using HellgroundArenaLog.BO.DTO;

namespace HellgroundArenaLog.UnitTests
{
  
    [TestClass()]
    public class ArmoryScannerTest
    {
        [TestMethod(), Owner("Rosiv")]
        public void Should_Retrieve_Top_40_Arena_Teams_From_Web_Armory()
        {
            ArmoryScanner target = new ArmoryScanner();
            ArenaTeam expected = null; // TODO: Initialize to an appropriate value
            ArenaTeam actual = target.Retrieve();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HellgroundArenaLog.BO;
using HellgroundArenaLog.BO.Providers;
using HellgroundArenaLog.BO.Providers.ArmoryScanner;
using HellgroundArenaLog.BO.DTO;

namespace HellgroundArenaLog.Client
{
    public partial class Form1 : Form
    {
        private ArmoryScanner armoryScanner;
        private List<ArenaTeam> top100;
        private Top100Comparer comparer;
        private Search searchForm;

        public Form1()
        {
            InitializeComponent();
        }

        private void intervalTextBox_TextChanged(object sender, EventArgs e)
        {
            this.timerTop100.Interval = Int32.Parse(this.intervalTextBox.Text) * 1000;
        }

        private void startStopButton_Click(object sender, EventArgs e)
        {
            if (this.startStopButton.Text == "Start")
            {
                this.startStopButton.Text = "Stop";
                StartScanning();
            }
            else
            {
                this.startStopButton.Text = "Start";
                StopScanning();
            }
        }

        private void StartScanning()
        {
            WriteMessage("Scanning started...");
            this.armoryScanner = new ArmoryScanner();
            this.timerTop100.Interval = Int32.Parse(this.intervalTextBox.Text) * 1000;
            this.timerTop100.Start();
            timerTop100_Tick(null, null);
        }

        private void StopScanning()
        {
            this.timerTop100.Stop();
            this.comparer = null;
            WriteMessage("Scanning stopped...");
        }

        private void timerTop100_Tick(object sender, EventArgs e)
        {
            try
            {
                WriteMessage("retrieving data from armory...");
                this.top100 = this.armoryScanner.Retrieve();
                WriteMessage("retrieving done...");
                if (this.comparer == null)
                {
                    this.comparer = new Top100Comparer(this.top100);
                }
                else
                {
                    WriteMessage("comparing results...");
                    comparer.CompareWithPrevious(this.top100);
                    List<ArenaTeam> diff = comparer.CurrentlyPlayingTeams;

                    if (diff.Count == 0)
                    {
                        WriteMessage("Currently playing: No one plays");
                    }

                    foreach (var arenaTeam in diff)
                    {
                        this.armoryScanner.RetrieveTeamDetails(arenaTeam);
                        string diffMessage = GetExtendedDiffMessage(arenaTeam);
                        WritePlayingMessage(GetBaseDiffMessage(arenaTeam));
                        WriteMessage(diffMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
        }

        private static string GetExtendedDiffMessage(ArenaTeam team)
        {
            string memberClasses = string.Empty;
            string memberNickNames = string.Empty;
            string ratingChange = string.Empty;

            foreach (var member in team.Members)
            {
                memberClasses += member.Class + " ";
                memberNickNames += member.NickName + " ";
            }

            return string.Format("Currently playing: [{0}] {1} {2} ({3}) what changed: {4}", team.Rating, memberClasses, memberNickNames, team.RateChange.ToString("+#;-#;0"), team.WhatChanged);
        }

        private static string GetBaseDiffMessage(ArenaTeam team)
        {
            string memberClasses = string.Empty;
            string memberNickNames = string.Empty;
            string ratingChange = string.Empty;

            foreach (var member in team.Members)
            {
                memberClasses += member.Class + " ";
                memberNickNames += member.NickName + " ";
            }

            return string.Format("Currently playing: [{0}] {1} {2} ({3})", team.Rating, memberClasses, memberNickNames, team.RateChange.ToString("+#;-#;0"));
        }

        private void WriteMessage(string message)
        {
            FileLogger.WriteDebugMessage(message);
            this.OutputTextBox.Text += string.Format("{0} {1} \r\n", DateTime.Now.ToString("HH:mm:ss"), message);
            // Scroll to end of text box
            this.OutputTextBox.SelectionStart = this.OutputTextBox.TextLength;
            this.OutputTextBox.ScrollToCaret();
        }

        private void WritePlayingMessage(string message)
        {
            FileLogger.WriteLogMessage(message);
            this.textBox1.Text += string.Format("{0} {1} \r\n", DateTime.Now.ToString("HH:mm:ss"), message);
            // Scroll to end of text box
            this.textBox1.SelectionStart = this.textBox1.TextLength;
            this.textBox1.ScrollToCaret();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timerTop100_Tick(null, null);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.searchForm == null || this.searchForm.IsDisposed)
            {
                this.searchForm = new Search();
            }

            this.searchForm.Show();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.notifyIcon1.Visible = true;
                this.notifyIcon1.ShowBalloonTip(250);
                this.Hide();
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                this.notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            RestoreWindow();
        }

        private void RestoreWindow()
        {
            this.Show();
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            RestoreWindow();
        }
    }
}

﻿using HellgroundArenaLog.BO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HellgroundArenaLog.Client
{
    public partial class Search : Form
    {
        public Search()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            SearchLogs();
        }

        private void SearchLogs()
        {
            this.textBox2.Text = string.Empty;
            string pattern = this.textBox1.Text;
            string logPath = HellgroundArenaLog.BO.FileLogger.LogPath;

            DirectoryInfo dirInfo = new DirectoryInfo(logPath);
            IEnumerable<FileInfo> files = dirInfo.EnumerateFiles();

            List<string> results = new List<string>();

            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (var file in files)
            {
                string[] lines = File.ReadAllLines(Path.Combine(logPath, file.Name));
                List<string> matchingLines =
                (from l in lines where l.IndexOf(pattern, StringComparison.InvariantCultureIgnoreCase) != -1 select string.Concat(DateFromFileName(file.Name), l)).ToList();
                results.AddRange(matchingLines);
            }
            sw.Stop();
            FileLogger.WriteDebugMessage(string.Format("searching log files recursively taken {0} ms", sw.ElapsedMilliseconds));

            StringBuilder sb = new StringBuilder();

            sw = new Stopwatch();
            sw.Start();
            foreach (var result in results)
            {
                sb.Append(result + "\r\n");
            }
            sw.Stop();
            FileLogger.WriteDebugMessage(string.Format("gathering results taken {0} ms", sw.ElapsedMilliseconds));
            this.textBox2.Text = sb.ToString();

        }

        private string DateFromFileName(string filename)
        {
            return string.Concat(filename.Substring(filename.Length - 12, 8).Insert(4, "-").Insert(7, "-")," ");
        }
    }
}
